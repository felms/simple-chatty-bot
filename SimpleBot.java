import java.util.Scanner;

public class SimpleBot {
    final static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        greet("BENDER", "2022");
        remindName();
        guessAge();
        count();
        test();
        end();
    }

    static void greet(String assistantName, String birthYear) {
        System.out.println("Hello! My name is " + assistantName + ".\n" +
                                "I was created in " + birthYear + ".\n" +
                                "Please, remind me your name.");
    }

    static void remindName() {
        String name = scanner.nextLine();
        System.out.println("What a great name you have, " + name + "!");
    }

    static void guessAge() {
        System.out.println("Let me guess your age.\n" +
                                "Enter remainders of dividing your age by 3, 5 and 7.");
        int remainder3 = scanner.nextInt();
        int remainder5 = scanner.nextInt();
        int remainder7 = scanner.nextInt();
        int age = (remainder3 * 70 + remainder5 * 21 + remainder7 * 15) % 105;
        System.out.println("Your age is " + age + "; that's a good time to start programming!");
    }

    static void count() {
        System.out.println("Now I will prove to you that I can count to any number you want.");
        int num = scanner.nextInt();
        for (int i = 0; i <= num; i++) {
            System.out.printf("%d!\n", i);
        }
    }

    static void test() {
        System.out.println("Let's test your programming knowledge.");
        int correctAnswer = 2;        
        System.out.println("Why do we use methods?\n" +
                                "1. To repeat a statement multiple times.\n" +
                                "2. To decompose a program into several small subroutines.\n" +
                                "3. To determine the execution time of a program.\n" +
                                "4. To interrupt the execution of a program.");
        
        int answer = scanner.nextInt();
        while(answer != correctAnswer) {
            System.out.println("Please, try again.");
            answer = scanner.nextInt();                
        }
    }

    static void end() {
        System.out.println("Congratulations, have a nice day!");
    }
}
